// License: LGPL-3.0 License (c) find-sec-bugs
package password

import com.hazelcast.config.SymmetricEncryptionConfig
import io.vertx.ext.web.handler.CSRFHandler
import java.net.PasswordAuthentication
import java.security.KeyStore
import java.sql.DriverManager
import javax.crypto.spec.PBEKeySpec
import javax.net.ssl.KeyManagerFactory
import javax.security.auth.callback.PasswordCallback
import javax.security.auth.kerberos.KerberosKey

class HardcodedPasswords {
    val passwordString: String = "secret"
    val passwordArray: CharArray = "secret".toCharArray()
    val passwordS = SymmetricEncryptionConfig()

    fun danger1(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.PasswordProtection("secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.PasswordProtection(passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.PasswordProtection(passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        KeyStore.PasswordProtection(password.toCharArray())
    }

    fun danger2(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.getInstance(null).load(null, "secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.getInstance(null).load(null, passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyStore.getInstance(null).load(null, passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        KeyStore.getInstance(null).load(null, password.toCharArray())
    }

    fun danger3(ks: KeyStore, password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        ks.load(null, "secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        ks.load(null, passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        ks.load(null, passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        ks.load(null, password.toCharArray())
    }

    fun danger4(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        KeyManagerFactory.getInstance(null).init(null, "secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyManagerFactory.getInstance(null).init(null, passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        KeyManagerFactory.getInstance(null).init(null, passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        KeyManagerFactory.getInstance(null).init(null, password.toCharArray())
    }

    fun danger5(kmf: KeyManagerFactory, password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        kmf.init(null, "secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        kmf.init(null, passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        kmf.init(null, passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        kmf.init(null, password.toCharArray())
    }

    fun danger6(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        PBEKeySpec("secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        PBEKeySpec(passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        PBEKeySpec(passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        PBEKeySpec(password.toCharArray())
    }

    fun danger7(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        PasswordAuthentication("user", "secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        PasswordAuthentication("user", passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        PasswordAuthentication("user", passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        PasswordAuthentication("user", password.toCharArray())
    }

    fun danger8(cb: PasswordCallback, password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        cb.setPassword("secret".toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        cb.setPassword(passwordString.toCharArray())

        // ruleid: kotlin_password_rule-HardcodePassword
        cb.setPassword(passwordArray)

        // ok: kotlin_password_rule-HardcodePassword
        cb.setPassword(password.toCharArray())
    }

    fun danger9(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        KerberosKey(null, "secret".toCharArray(), null)

        // ruleid: kotlin_password_rule-HardcodePassword
        KerberosKey(null, passwordString.toCharArray(), null)

        // ruleid: kotlin_password_rule-HardcodePassword
        KerberosKey(null, passwordArray, null)

        // ok: kotlin_password_rule-HardcodePassword
        KerberosKey(null, password.toCharArray(), null)
    }

    fun danger10(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "secret")

        // ruleid: kotlin_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", passwordString)

        // ruleid: kotlin_password_rule-HardcodePassword
        DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/test",
                "root",
                passwordArray.toString()
        )

        // ok: kotlin_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", password)
    }

    fun danger11(password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        CSRFHandler.create(null, "secret")

        // ruleid: kotlin_password_rule-HardcodePassword
        CSRFHandler.create(null, passwordString)

        // ruleid: kotlin_password_rule-HardcodePassword
        CSRFHandler.create(null, passwordArray.toString())

        // ok: kotlin_password_rule-HardcodePassword
        CSRFHandler.create(null, password)
    }

    fun danger12(s: SymmetricEncryptionConfig, password: String) {
        // ruleid: kotlin_password_rule-HardcodePassword
        s.setPassword("secret")

        // ruleid: kotlin_password_rule-HardcodePassword
        s.setPassword(passwordString)

        // ruleid: kotlin_password_rule-HardcodePassword
        s.setPassword(passwordArray.toString())

        // ok: kotlin_password_rule-HardcodePassword
        s.setPassword(password)
    }
}