# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/disallow-old-tls-versions2.yaml
# yamllint enable
---
rules:
  - id: java_crypto_rule-DisallowOldTLSVersion
    languages:
      - "java"
    patterns:
      - pattern: |
          $VALUE. ... .setProperty("jdk.tls.client.protocols", "$PATTERNS");
      - metavariable-pattern:
          metavariable: $PATTERNS
          language: generic
          patterns:
            - pattern-either:
                - pattern-regex: ^(.*TLSv1|.*SSLv.*)$
                - pattern-regex: ^(.*TLSv1,.*|.*TLSv1.1.*)
    message: |
      This application sets the `jdk.tls.client.protocols` system property to
      include insecure TLS or SSL versions (SSLv3, TLSv1, TLSv1.1), which are
      deprecated due to serious security vulnerabilities like POODLE attacks and
      susceptibility to man-in-the-middle attacks. Continuing to use these
      protocols can expose data to interception or manipulation. 

      To mitigate the issue, upgrade to TLSv1.2 or higher, which provide stronger 
      encryption and improved security. Refrain from using any SSL versions as they 
      are entirely deprecated.

      Secure Code Example:
      ```
      public void safe() {
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.3");
      }
      ```
    severity: "WARNING"
    metadata:
      likelihood: "MEDIUM"
      impact: "MEDIUM"
      confidence: "MEDIUM"
      shortDescription: "Inadequate encryption strength"
      category: "security"
      cwe: "CWE-326"
      owasp:
        - "A3:2017-Sensitive Data Exposure"
        - "A02:2021-Cryptographic Failures"
      security-severity: "MEDIUM"
      references:
        - "https://stackoverflow.com/questions/26504653/is-it-possible-to-disable-sslv3-for-all-java-applications"
      subcategory:
        - "vuln"
      technology:
        - "java"
      vulnerability: "Insecure Transport"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Mishandled Sensitive Information"
