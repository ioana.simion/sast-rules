# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# source (original): https://github.com/semgrep/semgrep-rules/blob/release/java/mongodb/security/injection/audit/mongodb-nosqli.yaml
# yamllint enable
---
rules:
  - id: java_inject_rule-MongodbNoSQLi
    languages:
      - java
    patterns:
      - pattern-either:
          - pattern: (com.mongodb.BasicDBObject $QUERY).put("$where", $INPUT);
          - pattern: |
              (HashMap<String, String> $MAP).put("$where", $INPUT);
              ...
              (com.mongodb.BasicDBObject $QUERY).putAll($MAP);
          - pattern: (com.mongodb.BasicDBObject $QUERY).append("$where", $INPUT);
          - pattern: new com.mongodb.BasicDBObject("$where", $INPUT);
          - pattern: |
              (HashMap<String, String> $MAP).put("$where", $INPUT);
              ...
              new com.mongodb.BasicDBObject($MAP);
          - pattern: |
              (HashMap<String, String> $MAP).put("$where", $INPUT);
              ...
              String json = new JSONObject($MAP).toString();
              ...
              (com.mongodb.BasicDBObject $QUERY).parse((String $JSON));
          - pattern: com.mongodb.BasicDBObjectBuilder.start().add("$where", $INPUT);
          - pattern: com.mongodb.BasicDBObjectBuilder.start().append("$where", $INPUT);
          - pattern: com.mongodb.BasicDBObjectBuilder.start("$where", $INPUT);
          - pattern: |
              (HashMap<String, String> $MAP).put("$where", $INPUT);
              ...
              com.mongodb.BasicDBObjectBuilder.start($MAP);
      - metavariable-pattern:
          metavariable: $INPUT
          patterns:
            - pattern: |
                ...
            - pattern-not: |
                "..."
    message: |
      Detected non-constant data passed into a NoSQL query using the 'where'
      evaluation operator. If this data can be controlled by an external user,
      this is a NoSQL injection. Ensure data passed to the NoSQL query is not
      user controllable, or properly sanitize the data. Ideally, avoid using the
      'where' operator at all and instead use the helper methods provided by
      com.mongodb.client.model.Filters with comparative operators such as eq,
      ne, lt, gt, etc.

      Secure Code Example:
      ```
      MongoDatabase database = mongoClient.getDatabase("mydb");
      MongoCollection<Document> collection = database.getCollection("users");

      // Secure usage with Filters.eq:
      String username = request.getParameter("username");
      collection.find(Filters.eq("username", username));
      ```
    severity: "ERROR"
    metadata:
      category: "security"
      technology:
        - "nosql"
        - "mongodb"
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      cwe: "CWE-943"
      shortDescription: "Improper neutralization of special elements in data query logic"
      security-severity: "CRITICAL"
      references:
        - "https://owasp.org/Top10/A03_2021-Injection"
        - "https://www.mongodb.com/docs/manual/tutorial/query-documents/"
        - "https://www.mongodb.com/docs/manual/reference/operator/query/where/"
      subcategory:
        - "audit"
      likelihood: "LOW"
      impact: "HIGH"
      confidence: "LOW"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
      vulnerability_class:
        - "Improper Validation"
