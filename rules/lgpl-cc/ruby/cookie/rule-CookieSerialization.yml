# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: ruby_cookie_rule-CookieSerialization
  message: |
    The rule warns of a significant security risk in applications that use 
    Ruby's Marshal module for deserializing cookies. Marshal can serialize 
    and deserialize Ruby objects, which, while powerful, poses a risk if 
    misused. If an attacker crafts a malicious cookie that is deserialized, 
    it could lead to remote code execution (RCE) on the server due to 
    Marshal.load's ability to execute code in deserialized objects. To mitigate 
    this risk, developers are advised to switch from Marshal to JSON for cookie 
    serialization. JSON is safer as it cannot execute arbitrary code, reducing 
    the threat of RCE vulnerabilities from deserialized user data. The hybrid 
    check is just to warn users to migrate to :json for best practice.
    
    Secure code example:
    ```
    # In Rails, configure the cookie serializer to :json
    # config/initializers/cookies_serializer.rb
    Rails.application.config.action_dispatch.cookies_serializer = :json
    ```

    Additionally, always validate and sanitize user-supplied data as much 
    as possible, even when using JSON, to further secure your application 
    against various injection attacks.
  languages:
  - "ruby"
  metadata:
    shortDescription: "Improper control of generation of code ('Code Injection')"
    category: "security"
    cwe: "CWE-94"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    technology:
    - "ruby"
    security-severity: "HIGH"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/cookie-serialization.yaml"
    - "https://robertheaton.com/2013/07/22/how-to-hack-a-rails-app-using-its-secret-token/"
  severity: "ERROR"
  pattern-either:
  - pattern: |
      Rails.application.config.action_dispatch.cookies_serializer = :marshal
  - pattern: |
      Rails.application.config.action_dispatch.cookies_serializer = :hybrid