# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_find_rule-CheckUnscopedFind"
  mode: taint
  pattern-sources:
  - pattern-either:
    - pattern: |
        cookies[...]
    - patterns:
      - pattern: |
          cookies. ... .$PROPERTY[...]
      - metavariable-regex:
          metavariable: $PROPERTY
          regex: (?!signed|encrypted)
    - pattern: |
        params[...]
    - pattern: |
        request.env[...]
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern: $MODEL.find(...)
      - pattern: $MODEL.find_by_id(...)
      - pattern: $MODEL.find_by_id!(...)
    - metavariable-regex:
        metavariable: $MODEL
        regex: '[A-Z]\S+'
  message: |
    The application was found calling the `find(...)` method with user-controlled input. If the 
    ActiveRecord model being searched against is sensitive, this may lead to 
    Insecure Direct Object Reference (IDOR) behavior and allow users to read 
    arbitrary records. This could lead to data breaches, including the 
    exposure of personal information, account takeovers, and other security 
    issues.

    To mitigate this risk, it's essential to scope queries to the current 
    user or another appropriate scope that ensures users can only access 
    data they are authorized to see. This is done by using ActiveRecord 
    associations and scopes to limit the records that can be retrieved. 

    Secure Code Example:
    ```
    # Secure way to scope the find to the current user's accounts
    def show
      @account = current_user.accounts.find(params[:id])
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-639"
    shortDescription: "Authorization bypass through user-controlled key"    
    references:
    - "https://brakemanscanner.org/docs/warning_types/unscoped_find/"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-unscoped-find.yaml"
    category: "security"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"