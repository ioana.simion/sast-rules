# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_mass_assignment_rule-ModelAttrAccessible"
  pattern-either:
  - pattern: |
      ....permit(..., :admin, ...)
  - pattern: |
      ....permit(..., :role, ...)
  - pattern: |
      ....permit(..., :banned, ...)
  - pattern: |
      ....permit(..., :account_id, ...)
  - pattern: |
      attr_accessible ..., :admin, ...
  - pattern: |
      attr_accessible ..., :role, ...
  - pattern: |
      attr_accessible ..., :banned, ...
  - pattern: |
      attr_accessible ..., :account_id, ...
  - pattern: |
      params.permit!
  message: |
    The application was found permitting attributes which could lead to mass assignment
    vulnerabilities. Permitting attributes such as `admin`, `role`, `banned` etc, 
    without proper authorization checks can lead to security issues like unauthorized 
    access or privilege escalation.

    Remediation Strategy
    - Explicitly permit attributes: Carefully review which attributes are 
    permissible and avoid including sensitive ones in the list.
    - Role-based permissions: Implement checks that allow only authorized 
    users (e.g., administrators) to modify sensitive attributes.
    - Avoid using `params.permit!`: Use specific permit statements instead of 
    permitting all parameters to ensure only expected attributes are allowed 
    for mass assignment.

    Secure Code Example:
    ```
    # Secure: Conditionally permit sensitive attributes based on user role
    def user_params
    permitted = params.require(:user).permit(:name, :email)
    permitted[:role] = params[:user][:role] if current_user.admin? && params[:user][:role].present?
    permitted
    end

    def user_params2
      permitted_attributes = [:username, :email]
      # Only allow 'admin' attribute to be updated by users with admin role
      permitted_attributes << :admin if current_user.admin?
      params.require(:user).permit(*permitted_attributes)
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    category: "security"
    shortDescription: "Improperly controlled modification of dynamically-determined object attributes"
    cwe: "CWE-915"
    owasp:
    - "A6:2017-Security Misconfiguration"
    - "A08:2021-Software and Data Integrity Failures"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"
    references:
    - "https://cheatsheetseries.owasp.org/cheatsheets/Mass_Assignment_Cheat_Sheet.html"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/model-attr-accessible.yaml"
