# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_injection_rule-DangerousExec"
  mode: taint
  pattern-sources:
  - patterns:
    - pattern: |
        def $F(...,$ARG,...)
            ...
        end
    - focus-metavariable: $ARG
  - pattern: params
  - pattern: cookies
  pattern-sinks:
  - patterns:
    - pattern: |
        $EXEC(...)
    - pattern-not: |
        $EXEC("...","...","...",...)
    - pattern-not: |
        $EXEC(["...","...","...",...],...)
    - pattern-not: |
        $EXEC({...},"...","...","...",...)
    - pattern-not: |
        $EXEC({...},["...","...","...",...],...)
    - metavariable-regex:
        metavariable: $EXEC
        regex: ^(system|exec|spawn|Process.exec|Process.spawn|Open3.capture2|Open3.capture2e|Open3.capture3|Open3.popen2|Open3.popen2e|Open3.popen3|IO.popen|Gem::Util.popen|PTY.spawn)$
  message: |
    OS command injection is a critical vulnerability that can lead to a full system
    compromise as it may allow an adversary to pass in arbitrary commands or arguments
    to be executed.

    User input should never be used in constructing commands or command arguments
    to functions which execute OS commands. This includes filenames supplied by
    user uploads or downloads.

    To mitigate this vulnerability, follow these best practices:
    - Validate input: Ensure that all user inputs are validated against a 
    strict pattern that only allows safe characters for the context. Reject 
    any input that does not meet these criteria.
    - Sanitize input: When validation is not feasible, sanitize the input 
    by escaping or removing potentially dangerous characters.
    - Use secure methods: Prefer using secure methods or libraries designed 
    to execute system commands with parameters, which automatically handle 
    proper escaping of user inputs, such as `Open3.capture3` in Ruby.
    - Least privilege: Run your application with the least privileges 
    necessary, limiting what an attacker can do if they manage to execute a 
    command.

    Secure Code Example:
    Instead of directly interpolating user input into system commands, validate 
    user input against an allowlist, then use parameterized execution or safer 
    APIs like Open3:
    ```
    require 'open3'

    user_input = params[:user_input]
    # Define an allow list of permitted arguments
    allowed_arguments = ['allowed_argument1', 'allowed_argument2', 'allowed_argument3']

    # Validate user_input against the list
    if allowed_arguments.include?(user_input)
      # Using Open3 to safely execute system commands with allowed arguments
      stdout, stderr, status = Open3.capture3('grep', user_input, 'my_file.txt')
      if status.success?
        puts stdout
      else
        warn "Error: #{stderr}"
      end
    else
      warn "Error: Unpermitted argument."
    end
    ```
  languages: 
  - "ruby"
  severity: "ERROR"
  metadata:
    shortDescription: "Improper control of generation of code ('Code Injection')"
    category: "security"
    cwe: "CWE-94"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    security-severity: "HIGH"
    technology:
    - "ruby"
    - "rails"
    references:
    - "https://guides.rubyonrails.org/security.html#command-line-injection"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/dangerous-exec.yaml"
    