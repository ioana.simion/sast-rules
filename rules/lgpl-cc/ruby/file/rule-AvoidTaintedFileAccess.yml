# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_file_rule-AvoidTaintedFileAccess"
  mode: taint
  pattern-sources:
  - pattern: params
  - pattern: cookies
  - pattern: request.env
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern: Dir.$X(...)
      - pattern: File.$X(...)
      - pattern: IO.$X(...)
      - pattern: Kernel.$X(...)
      - pattern: PStore.$X(...)
      - pattern: Pathname.$X(...)
    - metavariable-pattern:
        metavariable: $X
        patterns:
        - pattern-either:
          - pattern: chdir
          - pattern: chroot
          - pattern: delete
          - pattern: entries
          - pattern: foreach
          - pattern: glob
          - pattern: install
          - pattern: lchmod
          - pattern: lchown
          - pattern: link
          - pattern: load
          - pattern: load_file
          - pattern: makedirs
          - pattern: move
          - pattern: new
          - pattern: open
          - pattern: read
          - pattern: readlines
          - pattern: rename
          - pattern: rmdir
          - pattern: safe_unlink
          - pattern: symlink
          - pattern: syscopy
          - pattern: sysopen
          - pattern: truncate
          - pattern: unlink
  message: |
    The application dynamically constructs file or path information with user input. When an application uses data from 
    untrusted sources (like `params`, `cookies`, or `request.env`) to perform 
    actions on the file system, it opens up potential vulnerabilities. 
    A malicious actor could exploit this to access, modify, or delete 
    files they shouldn't have access to, leading to information disclosure, 
    data loss, or server compromise. This type of vulnerability is often 
    referred to as a File Inclusion vulnerability or Path Traversal attack, 
    depending on the nature of the exploit.

    To mitigate these risks, follow these best practices:
    - Validate and sanitize input: Ensure all user inputs are strictly 
    validated and sanitized to prevent path traversal characters (like `../`) 
    or other malicious patterns. Only allow filenames or paths that match 
    specific, safe patterns.
    - Use secure libraries: Utilize libraries or frameworks that abstract 
    file access in a secure manner, automatically handling the risks 
    associated with raw user input.
    - Least privilege principle: Run your application with the minimum 
    necessary file system permissions. Restrict the application's access 
    to only those directories and files it absolutely needs.
    - Directory whitelisting: Maintain an allow list of permitted directories 
    for file operations, rejecting any requests for paths outside of these 
    directories.

    Secure Code Example:
    ```
    def safe_file_read(filename)
      # Define a list of allowed files or use regex to validate the filename format
      allowed_files = ['allowed_file.txt', 'another_safe_file.txt']
      
      if allowed_files.include?(filename)
        file_path = Rails.root.join('safe_directory', filename)
        content = File.read(file_path)
        return content
      else
        raise "Access to the requested file is not allowed."
      end
    end
    ```
  languages: 
  - "ruby"
  severity: "WARNING"
  metadata:
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-22"
    shortDescription: "Improper limitation of a pathname to a restricted directory
        ('Path Traversal')"    
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/avoid-tainted-file-access.yaml"
    category: "security"
    security-severity: "MEDIUM"
    technology:
    - "rails"