#!/usr/bin/env bash

# Fetches ~SAST::Ruleset issues with missing priority label and attempts to
# identify a reference to a Semgrep rule in order to assign a priority label
# based on the rule's severity:
#
#  - ERROR -> ~SAST::Ruleset::P2
#  - WARNING -> ~SAST::Ruleset::P3
#  - INFO -> ~SAST::Ruleset::P4
#  - Other -> ~SAST::Ruleset::P4

set -e

# gitlab-org/gitlab
projectId=278964

# Issues must have ~SAST::Ruleset label assigned.
filterLabels="SAST::Ruleset"

# Issues must not have any priority labels assigned.
filterLabelsNot="SAST::Ruleset::P1,SAST::Ruleset::P2,SAST::Ruleset::P3,SAST::Ruleset::P4,"

# Performs an HTTP request to the URL provided as the first argument with
# optional method as second parameter (default GET).
#
# Access token is automatically added for requests to https://gitlab.com/ URLs.
function doRequest() {
  curlArgs=(
    "--request" "${2:-GET}" # request method from second argument or GET.
    "--location" # follow redirects.
    "--retry" "3" # retry failing requests up to 3 times.
    "--retry-delay" "10" # wait 10 seconds between each retry.
    "--silent" # only print response body.
    "--show-error" # show error if failing.
    "--fail" # fail fast on server error.
    "-g" # don't validate URLs (curl doesn't like the `&not[labels]=` query for some reason).
  )

  if [[ "$1" == "https://gitlab.com/"* ]]; then
    curlArgs+=("-H" "PRIVATE-TOKEN: $GITLAB_BOT_TOKEN")
  fi

  response="$(curl "${curlArgs[@]}" "$1" || true)"

  echo "$response"
}

# Attempts to find a reference to a Semgrep community rule in the text provided
# as the first argument and returns a URL to fetch the raw YAML.
function getCommunityRuleUrl() {
  url="$(echo "$1" | grep -m 1 -oE 'https:\/\/github\.com\/semgrep\/semgrep-rules\/[a-zA-Z0-9\.\/_-]+\.yaml' || true)"
  raw="${url//github\.com/raw.githubusercontent.com}"
  echo "${raw//blob\/}"
}

# Attempts to find a reference to a sast-rules rule in the text provided as the
# first argument and returns a URL to fetch the raw YAML.
function getRuleUrl() {
  path="$(echo "$1" | grep -m 1 -oE '(c|csharp|go|java|javascript|python|scala|lgpl|rules)[\/\\][a-zA-Z0-9\.\/\\_-]+\.(yml|yaml|c|cs|go|java|js|py|scala|kt|swift|m|rb)' || true)"

  if [ "$path" = "" ]; then
    return
  fi

  path="${path//\\//}" # convert backslashes to forward slashes.
  path="${path//-/_}" # convert all dashes to underscores.
  path="${path//rule_/rule-}" # convert rule prefix back to dash.

  # Add missing rules/ directory for lgpl paths.
  if [[ "$path" == "lgpl/"* ]]; then
    path="rules/$path"
  fi

  echo "https://gitlab.com/gitlab-org/security-products/sast-rules/-/raw/main/$(echo -n "$path" | cut -d . -f1).yml"
}

# Extracts the severity from the raw YAML of a rule provided as the first
# argument.
function getRuleSeverity() {
  sev="$(echo "$1" | grep -m 1 -E 'severity:\s+"?(ERROR|WARNING|INFO)' || true)"
  sev="$(echo "$sev" | grep -m 1 -oE '(ERROR|WARNING|INFO)' || true)"

  echo "$sev"
}

# Converts a Semgrep rule severity given as the first argument to its
# corresponding priority label.
#
# Defaults to P4 if severity is unknown.
function getPriorityLabel() {
  case "$1" in
    "ERROR")
        echo "SAST::Ruleset::P2"
        ;;
    "WARNING")
        echo "SAST::Ruleset::P3"
        ;;
    *)
        echo "SAST::Ruleset::P4"
        ;;
  esac
}

data="$(doRequest "https://gitlab.com/api/v4/projects/$projectId/issues/?state=opened&labels=$filterLabels&not[labels]=$filterLabelsNot&per_page=100")"

if [ "$data" = "" ]; then
    echo "No data returned from API"
    exit 1
fi

issues="$(echo "$data" | jq -c '.[]')"

echo "Fetched $(echo "$issues" | wc -l) issues without priority labels"

while IFS= read -r issue; do
  iid="$(echo "$issue" | jq -r '.iid')"
  haystack="$(echo "$issue" | jq -r '.title, .description')"
  url="$(getRuleUrl "$haystack")"

  if [ "$url" = "" ]; then
    url="$(getCommunityRuleUrl "$haystack")"
  fi

  if [ "$url" = "" ]; then
    echo "[#$iid]: issue does not contain any rule references"
    continue
  fi

  source="$(doRequest "$url")"

  if [ "$source" = "" ]; then
    echo "[#$iid]: ERROR: failed to get source from rule URL $url"
    continue
  fi

  sev="$(getRuleSeverity "$source")"
  prio="$(getPriorityLabel "$sev")"

  doRequest "https://gitlab.com/api/v4/projects/$projectId/issues/$iid?add_labels=$prio" "PUT" >> /dev/null

  echo "[#$iid]: assigned priority label $prio based on rule severity $sev"

done <<< "$issues"

